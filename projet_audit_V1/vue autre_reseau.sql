CREATE OR REPLACE VIEW public.autre_reseau
 AS
 SELECT type_2g.siret,
    type_2g.date_etude,
    type_2g.type_reseau,
    type_2g.bwg,
    type_2g.sfr,
    type_2g.obs,
    type_2g.opfree
   FROM type_2g
UNION ALL
 SELECT type_3g.siret,
    type_3g.date_etude,
    type_3g.type_reseau,
    type_3g.bwg,
    type_3g.sfr,
    type_3g.obs,
    type_3g.opfree
   FROM type_3g
UNION ALL
 SELECT type_4g.siret,
    type_4g.date_etude,
    type_4g.type_reseau,
    type_4g.bwg,
    type_4g.sfr,
    type_4g.obs,
    type_4g.opfree
   FROM type_4g;