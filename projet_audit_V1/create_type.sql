create table public.type_4g(
    siret varchar(14) PRIMARY KEY NOT NULL,
    date_etude date,
	type_reseau varchar(2),
    bwg varchar(5),
    sfr varchar(5),
    obs varchar(5),
    opfree varchar(5)
);

create table public.type_3g(
    siret varchar(14) PRIMARY KEY NOT NULL,
    date_etude date,
	type_reseau varchar(2),
    bwg varchar(5),
    sfr varchar(5),
    obs varchar(5),
    opfree varchar(5)
);

create table public.type_2g(
    siret varchar(14) PRIMARY KEY NOT NULL,
    date_etude date,
	type_reseau varchar(2),
    bwg varchar(5),
    sfr varchar(5),
    obs varchar(5),
    opfree varchar(5)
);