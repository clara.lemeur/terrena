﻿#import des fichiers shp dans la base
    function makeCompare {
        Param ($a, $b)
        $table = "'$a'"
        $operateur = "'$b'"
        $requete = '"SELECT makeCompare(' + $table + ', ' + $operateur + ');"'
        $compareCommande =  "$psql $param -c $requete"
        return  $compareCommande
    }

    function nonNull {
        Param ($latable)
        $requete = '"SELECT EXISTS (SELECT * FROM ' + $latable + ' LIMIT 1);"'
        return "$psql $param -t -c $requete"
    }

    $env:PGPASSWORD = 'Clara';
    $psql = "C:\'Program Files'\PostgreSQL\12\bin\psql.exe"
    $param = " -d postgis -h localhost -U postgres "
    $shp2pgsql = "C:\'Program Files'\PostgreSQL\12\bin\shp2pgsql.exe"
    $shp2 = ".\shp2pgsql.exe"
    $sql = ".\psql.exe"

    Set-Location C:\'Program Files'\PostgreSQL\12\bin
    
    #nom des tables
    $table2g = "type_2g"
    $table3g = "type_3g"
    $table4g = "type_4g"


    #============================================================2G=============================================================

    # Action manuelle, CF doc du point n°6
 
    #============================================================3G=============================================================
    # Insertion siret des sites dans table type
    $notNull = nonNull $table3g
    $notNull = Invoke-Expression $notNull
    if ($notNull -eq ' f'){
        Write-Host "EST VIDE" -ForegroundColor Green

        #import les siret dans la table
        $var = "'" + $table3g + "'"
        $requete = '"SELECT insert_sites(' + $var + ');"'
        $insert_siret = "$sql $param -c $requete"
        Invoke-Expression $insert_siret
    }


    #fichier 3g

    $3g = ".\3G\"
    $bwg3g = $3g + "'Bouygues Telecom'\METRO_Bouygues_Telecom_couv_3G_data_2019_T2.shp"
    $sfr3g = $3g + "SFR\METRO_SFR_couv_3G_data_2019_T2.shp"
    $obs3g = $3g + "Orange\METRO_OF_couv_3G_data_2019_T2.shp"
    $free3g = $3g + "'Free Mobile'\METRO_Free_Mobile_couv_3G_data_2019_T2.shp"


    #-----------------------------------------------------------BWG--------------------------------------------------------

    $importCommande = "$shp2 -s 2154 $bwg3g public.tampon | $sql $param"
    Invoke-Expression $importCommande 

    $makeCompare = makeCompare $table3g "bwg"
    Invoke-Expression $makeCompare

    #-----------------------------------------------------------SFR--------------------------------------------------------

    $importCommande = "$shp2 -s 2154 $sfr3g public.tampon | $sql $param"
    Invoke-Expression $importCommande 

    $makeCompare = makeCompare $table3g "sfr"
    Invoke-Expression $makeCompare

    #----------------------------------------------------------ORANGE------------------------------------------------------

    $importCommande = "$shp2 -s 2154 $obs3g public.tampon | $sql $param"
    Invoke-Expression $importCommande 

    $makeCompare = makeCompare $table3g "obs"
    Invoke-Expression $makeCompare

    #------------------------------------------------------------FREE------------------------------------------------------

    $importCommande = "$shp2 -s 2154 $free3g public.tampon | $sql $param"
    Invoke-Expression $importCommande 

    $makeCompare = makeCompare $table3g "free"
    Invoke-Expression $makeCompare

    Write-Host "3G Executer" -ForegroundColor Green

    #============================================================4G============================================================

    # Insertion siret des sites dans table type

    $notNull = nonNull $table4g
    $notNull = Invoke-Expression $notNull
    if ($notNull -eq ' f'){
        Write-Host "EST VIDE" -ForegroundColor Green

        #import les siret dans la table
        $var = "'" + $table4g + "'"
        $requete = '"SELECT insert_sites(' + $var + ');"'
        $insert_siret = "$sql $param -c $requete"
        Invoke-Expression $insert_siret
    }

    #fichier 4g

    $4g = ".\4G\"
    $bwg4g = $4g + "'Bouygues Telecom'\METRO_Bouygues_Telecom_couv_4G_data_2019_T2.shp"
    $sfr4g = $4g + "SFR\METRO_SFR_couv_4G_data_2019_T2.shp"
    $obs4g = $4g + "Orange\METRO_OF_couv_4G_data_2019_T2.shp"
    $free4g = $4g + "'Free Mobile'\METRO_Free_Mobile_couv_4G_data_2019_T2.shp"
 

    #---------------------------------------------------------------BWG--------------------------------------------------

    $importCommande = "$shp2 -s 2154 $bwg4g public.tampon | $sql $param"
    Invoke-Expression $importCommande 

    $makeCompare = makeCompare $table4g "bwg"
    Invoke-Expression $makeCompare

    #---------------------------------------------------------------SFR--------------------------------------------------

    $importCommande = "$shp2 -s 2154 $sfr4g public.tampon | $sql $param"
    Invoke-Expression $importCommande 

    $makeCompare = makeCompare $table4g "sfr"
    Invoke-Expression $makeCompare

    #-------------------------------------------------------------ORANGE-------------------------------------------------

    $importCommande = "$shp2 -s 2154 $obs4g public.tampon | $sql $param"
    Invoke-Expression $importCommande 

    $makeCompare = makeCompare $table4g "obs"
    Invoke-Expression $makeCompare

    #---------------------------------------------------------------FREE-------------------------------------------------

    $importCommande = "$shp2 -s 2154 $free4g public.tampon | $sql $param"
    Invoke-Expression $importCommande 

    $makeCompare = makeCompare $table4g "free"
    Invoke-Expression $makeCompare

    Write-Host "4G Executer" -ForegroundColor Green
