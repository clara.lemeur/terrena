set pause off;    
set feedback off;    
set newp 0;        
set showmode off;  
set verify off;    
set echo off;      
set term off;     
set linesize 2000;
set pagesize 0; 
set trimspool on;

spool D:\Siren\1_Siren_Siret\ListeSiren.csv

select 'Source;Siren;Perimetre_Groupe;Date_integration' from dual;

select source_des_siren || ';' || siren || ';' || societe_integree_integralement || ';' || date_integration_siren  from autre_siren;

spool off;

exit;

