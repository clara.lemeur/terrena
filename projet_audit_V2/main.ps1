﻿


#variable global
$env:PGPASSWORD = 'Clara'
$psql = "C:\'Program Files'\PostgreSQL\12\bin\psql.exe"
$param = " -d postgis -h localhost -U postgres "


# 1. Actualisation de la table Siret depuis OpenData Gouv
.\1_Siren_Siret\Siren.ps1


# 2. Import dans un CSV de tous les siret Terrena avec coordonnées GPS
.\2_Generation_Sites_Terrena\export_site_from_oracle.ps1   


# 3. Création ou remplacement des fonctions makepoint, makecompare et insert_sites dans la base PostGis
$cmd = "$psql $param -f '.\3_Creation_fonctions_PostGis\fonctions.sql'"
Invoke-Expression $cmd


# 4. Création des tables type 2G, 3G et 4G
$cmd = "$psql $param -f '.\4_Creation_tables_2G_3G_4G\create_tables.sql'"
Invoke-Expression $cmd

# 5. Intégration des Sites dans la base PostGis (table Sites et initialisation des tables 2G, 3g et 4G)
.\5_Integration_Sites_PostGis\integration_sites_to_postgis.ps1
    

# 6. Information sur l'action manuelle à réaliser pour intégrer les données dans la table 2G
Add-Type -AssemblyName PresentationCore,PresentationFramework 
$result = [System.Windows.MessageBox]::Show("Opération manuelle pour le reseau 2G (cf la doc)","Operation Completed",[System.Windows.MessageBoxButton]::ok,[System.Windows.MessageBoxImage]::Warning)


# 7. Intégration automatique des SHP 3G et 4G
.\7_Integration_automatique_shp_3G_4G\import_3G_4G_to_postgis.ps1


# 8. Export des données des tables 2G, 3G et 4G dans un fichier CSV
.\8_Export_CSV_donnees_2G_3G_4G\export_donnees_2G_3G_4G.ps1


# 9. Intégration des données dans Agil'IT
.\9_Integration_AgilIT\Integration_donnees_agilIT.ps1

