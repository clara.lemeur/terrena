﻿

# SqlPlus, SqlLoader
$Sql_Plus = "C:\ORA122\bin\sqlplus.exe"
$Sql_Loader = "C:\ORA122\bin\sqlldr.exe"
$Id_Sql = "gesttel/telagilp1120@agilp"

# Intégration des données des Devices
$fic_control = ".\Integration_donnees_agilIT.lcf"
$fic_log = ".\Integration_donnees_agilIT.log"
$cmd = "$sql_loader $id_sql control=$fic_control log=$fic_log direct=y errors=100 skip=1"
Invoke-Expression $cmd