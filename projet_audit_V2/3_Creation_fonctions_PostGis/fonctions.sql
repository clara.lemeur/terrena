

-- Insert_sites: insert dans les tables 2G, 3G, 4G, les Siret des sites

CREATE OR REPLACE FUNCTION insert_sites(_tbl regclass)
RETURNS VOID
AS $BODY$
DECLARE
	vgid INTEGER;
	vsiret varchar;
	curseur_id CURSOR FOR SELECT gid FROM sites;
BEGIN	
	OPEN curseur_id;
	LOOP
		FETCH curseur_id INTO vgid;
		EXIT WHEN NOT FOUND;
		SELECT siret INTO vsiret FROM sites WHERE gid = vgid;
		-- on insert dans la table passer en parametre
		EXECUTE format('INSERT INTO public.%s (siret) VALUES (%s);', _tbl, vsiret);
	END LOOP;
	CLOSE curseur_id;
END;
$BODY$
LANGUAGE plpgsql;





-- MakePoint: Crée les géométrie des sites en fonction de leur coordonnées

CREATE OR REPLACE FUNCTION public.makepoint()
RETURNS VOID
AS $BODY$
DECLARE
	long float;
	lat float;
   	nbrow bigint;
	i bigint = 0;
BEGIN
	SELECT count(*) INTO nbrow FROM sites;
    FOR i in 0 .. nbrow 
	LOOP
		SELECT cast(sites.longitude as float), cast(sites.latitude as float) INTO long, lat FROM sites WHERE gid = i;
		UPDATE public.sites SET geom = ST_Transform(ST_SetSRID( ST_Point(lat, long), 4326) , 2154) WHERE gid = i;
    END LOOP;
END;
$BODY$
LANGUAGE plpgsql;

ALTER FUNCTION public.makepoint()
    OWNER TO postgres;





-- Makecompare: Compare les points (coordonnées GPS des sites) aux multipolygones (cartes SHP de l'Arcep)

CREATE OR REPLACE FUNCTION makeCompare(_tbl regclass, ope varchar) 
RETURNS VOID
AS $BODY$
DECLARE
	vgid INTEGER;
	vsiret varchar;
	vdept varchar;
	vdate_etude date;
	vtype varchar(2);
	operateur name;
	unPoint geometry;
	unPolygone geometry;
	curseur_id CURSOR FOR SELECT gid FROM public.sites;
	vniveau varchar(5);
	curseur2g REFCURSOR;
	rec RECORD;
BEGIN	
	EXECUTE format('SELECT column_name FROM information_schema.columns WHERE table_name=%L AND column_name like %L;',_tbl , format('%s%s%s', '%', ope, '%')) INTO operateur;
	OPEN curseur_id;
	LOOP
		FETCH curseur_id INTO vgid;
		EXIT WHEN NOT FOUND;
		-- on recuperer les donnees geometry
		SELECT geom, siret, dept INTO unPoint, vsiret, vdept FROM public.sites WHERE gid = vgid;
		SELECT geom, date, techno INTO unPolygone, vdate_etude, vtype FROM public.tampon WHERE dept = vdept;
		-- si type 2g
		IF (vtype = '2G') THEN
			OPEN curseur2g FOR SELECT niveau, geom FROM public.tampon WHERE dept = vdept;
			LOOP
				FETCH curseur2g INTO rec;
				EXIT WHEN NOT FOUND;
				IF (st_contains(rec.geom, unPoint) = 't') THEN
					vniveau:=rec.niveau;
				ElSE IF (unPoint is null) THEN
					vniveau:='';
				END IF;
				END IF;
			END LOOP;
			CLOSE curseur2g;
			EXECUTE format('UPDATE public.%s set %s = %L, date_etude = %L, type_reseau = %L WHERE siret = %L;', _tbl, operateur, format('%s', vniveau), cast(vdate_etude as date), format('%s', vtype), format('%s', vsiret));
		ELSE 
			EXECUTE format('UPDATE public.%s set %s = %L, date_etude = %L, type_reseau = %L WHERE siret = %L;', _tbl, operateur, format('%s', st_contains(unPolygone, unPoint)), cast(vdate_etude as date), format('%s', vtype), format('%s', vsiret));
		END IF;
	END LOOP;
	CLOSE curseur_id;
	DROP TABLE public.tampon;
END;
$BODY$
LANGUAGE plpgsql;


