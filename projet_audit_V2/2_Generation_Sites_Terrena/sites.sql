set pause off;    
set feedback off;    
set newp 0;        
set showmode off;  
set verify off;    
set echo off;      
set term off;     
set linesize 2000;
set pagesize 0; 
set trimspool on;

spool D:\Siren\2_Generation_Sites_Terrena\sites.csv;

select 'siret;dept;latitude;longitude' from dual;

select siret || ';' ||
substr(CODEPOSTALETABLISSEMENT,0,2) || ';' ||
substr(GEOLOCETABLISSEMENT, instr(GEOLOCETABLISSEMENT, ' ')+1, length(GEOLOCETABLISSEMENT)) || ';' ||
substr(GEOLOCETABLISSEMENT, 0, instr(GEOLOCETABLISSEMENT, ' ')-1)
from autre_siret
where GEOLOCETABLISSEMENT is not null;

spool off;

exit;