﻿
# import des sites dans la base

#variable global
$env:PGPASSWORD = 'Clara';
$psql = "C:\'Program Files'\PostgreSQL\12\bin\psql.exe"
$param = " -d postgis -h localhost -U postgres "
        

function importsites {
    Param ($notNull)
    if ($notNull -eq ' f'){
        Write-Host "MAIS EST VIDE" -ForegroundColor Green

        #import les sites dans la base
        $fileSites = '..\2_Generation_Sites_Terrena\sites.csv'
        $cmd = '"\copy sites(siret,dept,latitude,longitude) FROM ' + $fileSites + ' WITH (FORMAT CSV, DELIMITER' + "';'"  + ', HEADER ' + '1' + ') ;"'
        $importSites = "$psql $param -c $cmd"
        Invoke-Expression $importSites

        #créer les geometry des points
        $makepoint = $psql + $param + '-c "SELECT makepoint();"'
        Invoke-Expression $makepoint
    }
}


# Variable pour verifier si table Sites existe

$sites = "'sites'"
$requete = '"SELECT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where table_name = ' + $sites + ');"'
$cmd = "$psql $param -t -c $requete"
$cmd = Invoke-Expression $cmd

#variable pour verif si table sites n'est pas vide
$requete = '"SELECT EXISTS (SELECT * FROM sites LIMIT 1);"'
$notNull = "$psql $param -t -c $requete"

Write-Host $testA -ForegroundColor Magenta


# Si la table Site n'existe pas on la crée, si elle existe on intégre les données avec tous nos Siret

if ($cmd -eq ' f'){
    Write-Host "EXISTE PAS" -ForegroundColor Red
    $cmd = "$psql $param -f '.\create_table_sites.sql'"
    Invoke-Expression $cmd

    $notNull = Invoke-Expression $notNull
    importsites $notNull
}
else { 
    Write-Host "EXISTE" -ForegroundColor Green
    $notNull = Invoke-Expression $notNull
    importsites $notNull
}




## Insertion dans les tables 2G, 3G et 4G des données de la table Site




#nom des tables
$table2g = "type_2g"
$table3g = "type_3g"
$table4g = "type_4g"


function nonNull {
    Param ($latable)
    $requete = '"SELECT EXISTS (SELECT * FROM ' + $latable + ' LIMIT 1);"'
    return "$psql $param -t -c $requete"
}


# Table 2G

    $notNull = nonNull $table2g
    $notNull = Invoke-Expression $notNull
    if ($notNull -eq ' f'){
        Write-Host "EST VIDE" -ForegroundColor Green

        #import les siret dans la table
        $var = "'" + $table2g + "'"
        $requete = '"SELECT insert_sites(' + $var + ');"'
        $insert_siret = "$psql $param -c $requete"
        Invoke-Expression $insert_siret
    }


# Table 3G

    $notNull = nonNull $table3g
    $notNull = Invoke-Expression $notNull
    if ($notNull -eq ' f'){
        Write-Host "EST VIDE" -ForegroundColor Green

        #import les siret dans la table
        $var = "'" + $table3g + "'"
        $requete = '"SELECT insert_sites(' + $var + ');"'
        $insert_siret = "$psql $param -c $requete"
        Invoke-Expression $insert_siret
    }


# Table 4G

    $notNull = nonNull $table4g
    $notNull = Invoke-Expression $notNull
    if ($notNull -eq ' f'){
        Write-Host "EST VIDE" -ForegroundColor Green

        #import les siret dans la table
        $var = "'" + $table4g + "'"
        $requete = '"SELECT insert_sites(' + $var + ');"'
        $insert_siret = "$psql $param -c $requete"
        Invoke-Expression $insert_siret
    }


