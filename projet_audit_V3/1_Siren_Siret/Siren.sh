#!/bin/bash
# Siren.sh

ListeSirenSql = "./ListeSiren.sql"

# SqlPlus, SqlLoader
Sql_Plus = "/ORA122/bin/sqlplus.exe"
Sql_Loader = "/ORA122/bin/sqlldr.exe"
Id_Sql = "gesttel/telagilp1120@agilp"


cmd = $Sql_Plus + " " + $Id_Sql + ' @' +  $ListeSirenSql
Invoke-Expression $cmd


$fichierCsvExport = "ListeSiren.csv"
$OutFile = "./$fichierCsvExport"


$Siren = Import-csv -Path $OutFile -Delimiter ';'

$Query = "q="
foreach($s in $Siren.siren){
           $Query = $Query + "siren=" + $s + "+or+"
}

$Query = $Query.Substring(0, $Query.Length-4)
$UrlOpenDataSoft = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=sirene_v3"
$Variables = "rows=10000&sort=datederniertraitementetablissement&facet=statutdiffusionetablissement&facet=trancheeffectifsetablissement&facet=etablissementsiege&facet=libellecommuneetablissement&facet=etatadministratifetablissement&facet=nomenclatureactiviteprincipaleetablissement&facet=caractereemployeuretablissement&facet=departementetablissement&facet=regionetablissement&facet=sectionetablissement&facet=classeetablissement&facet=unitepurgeeunitelegale&facet=sexeunitelegale&facet=trancheeffectifsunitelegale&facet=categorieentreprise&facet=sectionunitelegale&facet=classeunitelegale&facet=naturejuridiqueunitelegale"
$UrlComplete = "$UrlOpenDataSoft&$Query&$Variables"

$Sirets = Invoke-RestMethod -Uri $UrlComplete

$OutFileOpenDataSoft = "./Siret.csv"
Remove-Item $OutFileOpenDataSoft
$EnTete = 'etatadministratifunitelegale;siretsiegeunitelegale;siren;datecreationunitelegale;denominationusuelle1unitelegale;etablissementsiege;enseigne1etablissement;etatadministratifetablissement;siret;datecreationetablissement;adresseetablissement;codepostaletablissement;libellecommuneetablissement;geolocetablissement;datedebutetablissement;datefermetureetablissement;activiteprincipaleetablissement;nomenclatureactiviteprincipaleetablissement;nic;nicsiegeunitelegale;codeepcietablissement;trancheeffectifsunitelegale;anneeeffectifsunitelegale;divisionetablissement;classeetablissement;groupeetablissement;soussectionetablissement;divisionunitelegale;sectionunitelegale;groupeunitelegale;naturejuridiqueunitelegale;sectionetablissement;soussectionunitelegale;classeunitelegale;activiteprincipaleunitelegale;denominationunitelegale;l1_adressage_unitelegale;numerovoieetablissement;typevoieetablissement;libellevoieetablissement;codearrondissementetablissement;epcietablissement;codecommuneetablissement;codedepartementetablissement;departementetablissement;coderegionetablissement;regionetablissement;altitudemoyennecommuneetablissement;caractereemployeurunitelegale;populationcommuneetablissement;superficiecommuneetablissement;statutdiffusionunitelegale;nombreperiodesetablissement;categorieentreprise;categoriejuridiqueunitelegale;economiesocialesolidaireunitelegale;nomenclatureactiviteprincipaleunitelegale;caractereemployeuretablissement;anneecategorieentreprise;statutdiffusionetablissement;datederniertraitementunitelegale'
Set-Content -Encoding UTF8 -Value $EnTete -Path $OutFileOpenDataSoft



foreach($siret in $Sirets.records.fields){

 $ligne = ''

# Consolidation données
        $ligne = $Siret.etatadministratifunitelegale + ';' + ` 

        $Siret.siretsiegeunitelegale + ';' + ` 
        $Siret.siren + ';' + ` 
        $Siret.datecreationunitelegale + ';' + ` 
        $Siret.denominationusuelle1unitelegale + ';' + ` 
        $Siret.etablissementsiege + ';' + ` 
        $Siret.enseigne1etablissement + ';' + ` 
        $Siret.etatadministratifetablissement + ';' + ` 
        $Siret.siret + ';' + ` 
        $Siret.datecreationetablissement + ';' + ` 
        $Siret.adresseetablissement + ';' + ` 
        $Siret.codepostaletablissement + ';' + ` 
        $Siret.libellecommuneetablissement + ';' + ` 
        $Siret.geolocetablissement + ';' + ` 
        $Siret.datedebutetablissement + ';' + ` 
        $Siret.datefermetureetablissement + ';' + ` 
        $Siret.activiteprincipaleetablissement + ';' + ` 
        $Siret.nomenclatureactiviteprincipaleetablissement + ';' + ` 
        $Siret.nic + ';' + ` 
        $Siret.nicsiegeunitelegale + ';' + ` 
        $Siret.codeepcietablissement + ';' + ` 
        $Siret.trancheeffectifsunitelegale + ';' + ` 
        $Siret.anneeeffectifsunitelegale + ';' + ` 
        $Siret.divisionetablissement + ';' + ` 
        $Siret.classeetablissement + ';' + ` 
        $Siret.groupeetablissement + ';' + ` 
        $Siret.soussectionetablissement + ';' + ` 
        $Siret.divisionunitelegale + ';' + ` 
        $Siret.sectionunitelegale + ';' + ` 
        $Siret.groupeunitelegale + ';' + ` 
        $Siret.naturejuridiqueunitelegale + ';' + ` 
        $Siret.sectionetablissement + ';' + ` 
        $Siret.soussectionunitelegale + ';' + ` 
        $Siret.classeunitelegale + ';' + ` 
        $Siret.activiteprincipaleunitelegale + ';' + ` 
        $Siret.denominationunitelegale + ';' + ` 
        $Siret.l1_adressage_unitelegale + ';' + ` 
        $Siret.numerovoieetablissement + ';' + ` 
        $Siret.typevoieetablissement + ';' + ` 
        $Siret.libellevoieetablissement + ';' + ` 
        $Siret.codearrondissementetablissement + ';' + ` 
        $Siret.epcietablissement + ';' + ` 
        $Siret.codecommuneetablissement + ';' + ` 
        $Siret.codedepartementetablissement + ';' + ` 
        $Siret.departementetablissement + ';' + ` 
        $Siret.coderegionetablissement + ';' + ` 
        $Siret.regionetablissement + ';' + ` 
        $Siret.altitudemoyennecommuneetablissement + ';' + ` 
        $Siret.caractereemployeurunitelegale + ';' + ` 
        $Siret.populationcommuneetablissement + ';' + ` 
        $Siret.superficiecommuneetablissement + ';' + ` 
        $Siret.statutdiffusionunitelegale + ';' + ` 
        $Siret.nombreperiodesetablissement + ';' + ` 
        $Siret.categorieentreprise + ';' + ` 
        $Siret.categoriejuridiqueunitelegale + ';' + ` 
        $Siret.economiesocialesolidaireunitelegale + ';' + ` 
        $Siret.nomenclatureactiviteprincipaleunitelegale + ';' + ` 
        $Siret.caractereemployeuretablissement + ';' + ` 
        $Siret.anneecategorieentreprise + ';' + ` 
        $Siret.statutdiffusionetablissement + ';' + ` 
        $Siret.datederniertraitementunitelegale


Add-Content -Encoding UTF8 -Value $ligne.ToString() -Path $OutFileOpenDataSoft



}

   # Intégration des données des Devices
        $fic_control = "./Siret.lcf"
        $fic_log = "./Siret.log"
        $cmd = $sql_loader + " " + $id_sql + " " + "control=$fic_control log=$fic_log direct=y rrors=100 skip=1"
        Invoke-Expression $cmd
