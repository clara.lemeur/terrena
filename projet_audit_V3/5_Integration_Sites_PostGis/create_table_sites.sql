

 CREATE TABLE IF NOT EXISTS public.sites(
	 gid serial,
	 siret character varying(254) COLLATE pg_catalog."default",
	 dept character varying(254) COLLATE pg_catalog."default",
	 latitude character varying(254) COLLATE pg_catalog."default",
	 longitude character varying(254) COLLATE pg_catalog."default",
	 geom geometry(Point,2154),
	 CONSTRAINT siret_pkey PRIMARY KEY (siret)
)