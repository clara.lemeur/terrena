#!/bin/bash
# integration_sites_to_postgis.sh
# import des sites dans la base


#variable global
varpsql="/usr/bin/psql"
param=" -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur "

# Si la table Site n'existe pas on la crée, si elle existe on intégre les données avec tous nos Siret
if [ "$(psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -t -c "SELECT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where table_name = 'sites');" | cut -c 2- | grep 'f')" = "f" ];
then
    echo -e "\e[31mLA TABLE SITES EXISTE PAS"
    psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -f './create_table_sites.sql'
else
    echo -e "\e[32mLA TABLE SITES EXISTE"
fi

if [ "$( psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT EXISTS (SELECT * FROM SITES LIMIT 1);" | cut -c 2- | grep 'f')" = 'f' ]
then
    echo -e "\E[32MMAIS EST VIDE"
    #importe les sites dans la base
    psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "\COPY SITES(SIRET,DEPT,LATITUDE,LONGITUDE) FROM '../2_Generation_Sites_Terrena/sites.csv' WITH (FORMAT CSV, DELIMITER ';' , HEADER 1);"
    
    #CRÉER LES GEOMETRY DES POINTS
    psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT makepoint();"
fi



## Insertion dans les tables 2G, 3G et 4G des données de la table Site

# Table 2G
if [ "$(psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT EXISTS (SELECT * FROM type_2g LIMIT 1);"  | cut -c 2- | grep 'f')" = "f" ]
then
    echo -e  "\e[32mTYPE_2G EST VIDE"
    #import les siret dans la table
    psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT insert_sites('type_2g');"
fi


# Table 3G
if [ "$(psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT EXISTS (SELECT * FROM type_3g LIMIT 1);"  | cut -c 2- | grep 'f')" = "f" ]
then
    echo -e "\e[32mTYPE_3G EST VIDE"
    #import les siret dans la table
    psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT insert_sites('type_3g');"
fi


# Table 4G
if [ "$(psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT EXISTS (SELECT * FROM type_4g LIMIT 1);" | cut -c 2- | grep 'f')" = "f" ]
then
    echo -e "\e[32mTYPE_4G EST VIDE"
    #import les siret dans la table
    psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT insert_sites('type_4g');"
fi
