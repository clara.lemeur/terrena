#!/bin/bash
# import_3G_4G_to_postgis.sh 

#import des fichiers shp dans la base

PGPASSWORD='P@ssword'
psql="/usr/bin/psql"
param=" -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur "
shp2pgsql="/usr/bin/shp2pgsql"

#============================================================2G=============================================================

# Action manuelle, CF doc du point n°6

#============================================================3G=============================================================
# Insertion siret des sites dans table type
varnonNull=$($psql$param'-t -c "SELECT EXISTS (SELECT * FROM type_3g LIMIT 1"')
#psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -t -c "SELECT EXISTS (SELECT * FROM type_3g LIMIT 1);"
#echo -e "\e[34m"$varnotNull
if [ $varnotNull=" f" ]
then
    echo -e "\e[32mEST VIDE"
    #import les siret dans la table
    psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT insert_sites('type_3g');"
fi

#fichier 3g
la3g="./3G/"
bwg3g=$la3g"'Bouygues Telecom'/METRO_Bouygues_Telecom_couv_3G_data_2019_T2.shp"
sfr3g=$la3g"SFR/METRO_SFR_couv_3G_data_2019_T2.shp"
obs3g=$la3g"Orange/METRO_OF_couv_3G_data_2019_T2.shp"
free3g=$la3g"'Free Mobile'/METRO_Free_Mobile_couv_3G_data_2019_T2.shp"

#-----------------------------------------------------------BWG--------------------------------------------------------

importCommande=$shp2" -s 2154 "$bwg3g" public.tampon | "$psql$param
#shp2pgsql -s 2154 ./3G/'Bouygues Telecom'\METRO_Bouygues_Telecom_couv_3G_data_2019_T2.shp public.tampon | ./psql.exe  -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur
echo -e "\e[35m" $importCommande 

psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT makeCompare('type_3g','bwg');"
echo -e "\e[35m" $makeCompare

#-----------------------------------------------------------SFR--------------------------------------------------------

$importCommande= $shp2" -s 2154 "$sfr3g" public.tampon | "$psql$param

psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT makeCompare('type_3g','sfr');"
#----------------------------------------------------------ORANGE------------------------------------------------------

$importCommande=$shp2" -s 2154 "$obs3g "public.tampon | "$psql$param

psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT makeCompare('type_3g','obs');"

#------------------------------------------------------------FREE------------------------------------------------------

$importCommande=$shp2" -s 2154 "$free3g" public.tampon | "$psql$param
echo -e "\e[35m" $importCommande

psql -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur -c "SELECT makeCompare('type_3g','free');"

echo -e "\e[32m" "3G Executer"

#============================================================4G============================================================

# Insertion siret des sites dans table type

$varnotNull= nonNull $table4g
echo -e "\e[35m" $notNull
#$notNull = Invoke-Expression $notNull
if [ $varnotNull=" f" ]
then
    echo -e "\e[35m" "EST VIDE"
    
    #import les siret dans la table
    $var="'"$table4g"'"
    $requete='"SELECT INSERT_SITES('$table4g');"'
    $insert_siret=$psql$param" -c "
    echo -e "\e[35m" $insert_siret
fi

#fichier 4g

la4g="./4G/"
bwg4g=$la4g"'Bouygues Telecom'/METRO_Bouygues_Telecom_couv_4G_data_2019_T2.shp"
sfr4g=$la4g"SFR/METRO_SFR_couv_4G_data_2019_T2.shp"
obs4g=$la4g"Orange/METRO_OF_couv_4G_data_2019_T2.shp"
free4g=$la4g"'Free Mobile'/METRO_Free_Mobile_couv_4G_data_2019_T2.shp"


#---------------------------------------------------------------BWG--------------------------------------------------

$importCommande=$shp2" -s 2154 "$bwg4g" public.tampon | "$psql$param
echo -e "\e[35m" $importCommande 

$makeCompare= makeCompare $table4g "bwg"
echo -e "\e[35m" $makeCompare

#---------------------------------------------------------------SFR--------------------------------------------------

$importCommande=$shp2" -s 2154 "$sfr4g" public.tampon | "$psql$param
echo -e "\e[35m" $importCommande 

$makeCompare= makeCompare $table4g "sfr"
echo -e "\e[35m" $makeCompare

#-------------------------------------------------------------ORANGE-------------------------------------------------

$importCommande=$shp2" -s 2154 "$obs4g" public.tampon | "$psql$param
echo -e "\e[35m" $importCommande 

$makeCompare= makeCompare $table4g "obs"
echo -e "\e[35m" $makeCompare

#---------------------------------------------------------------FREE-------------------------------------------------

$importCommande=$shp2"-s 2154 "$free4g"public.tampon | "$psql$param
echo -e "\e[35m" $importCommande 

$makeCompare= makeCompare $table4g "free"
echo -e "\e[35m" $makeCompare

echo -e "\e[32m" "4G Executer"
