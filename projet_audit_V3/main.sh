#!/bin/bash
#main.sh


#variable global
psql="/usr/bin/psql"
param=" -d c.lemeur -h postgresql.bts-malraux72.net -U c.lemeur "

# 1. Actualisation de la table Siret depuis OpenData Gouv
./1_Siren_Siret/Siren.sh

# 2. Import dans un CSV de tous les siret Terrena avec coordonnées GPS
./2_Generation_Sites_Terrena/export_site_from_oracle.sh   

# 3. Création ou remplacement des fonctions makepoint, makecompare et insert_sites dans la base PostGis
cmd=$psql$param" -f ./3_Creation_fonctions_PostGis/fonctions.sql"
$cmd

# 4. Création des tables type 2G, 3G et 4G
cmd=$psql$param" -f ./4_Creation_tables_2G_3G_4G/create_tables.sql"
$cmd

# 5. Intégration des Sites dans la base PostGis (table Sites et initialisation des tables 2G, 3g et 4G)
./5_Integration_Sites_PostGis/integration_sites_to_postgis.sh

# 6. Information sur l'action manuelle à réaliser pour intégrer les données dans la table 2G
echo "Opération manuelle pour le reseau 2G (cf la doc)"

# 7. Intégration automatique des SHP 3G et 4G
./7_Integration_automatique_shp_3G_4G/import_3G_4G_to_postgis.sh

# 8. Export des données des tables 2G, 3G et 4G dans un fichier CSV
./8_Export_CSV_donnees_2G_3G_4G/export_donnees_2G_3G_4G.sh 

# 9. Intégration des données dans Agil'IT
./9_Integration_AgilIT/Integration_donnees_agilIT.sh 
