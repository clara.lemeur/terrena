﻿#!/bin/bash
# export_donnees_2G_3G_4G.sh

$env:PGPASSWORD = 'Clara';
$psql = "C:/'Program Files'/PostgreSQL/12/bin/psql.exe"
$param = " -U postgres -w -d postgis "


## Création de la vue
$cmd = "$psql $param -f './Vue.sql'"
Invoke-Expression $cmd

#Export des données
$file = "'./export_donnees_2G_3G_4G.csv'"
$cmd = '"\copy (SELECT * FROM autre_reseau) TO  ' + $file + ' WITH (FORMAT CSV, DELIMITER' + "';'"  + ', HEADER);"'

$exportResultat = $psql + $param + "-c "+  $cmd
Invoke-Expression $exportResultat
