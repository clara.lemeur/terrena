




-- Création de la table 2G

	CREATE TABLE IF NOT EXISTS public.type_2g(
		siret varchar(14) PRIMARY KEY NOT NULL,
		date_etude date,
		type_reseau varchar(2),
		bwg varchar(5),
		sfr varchar(5),
		obs varchar(5),
		opfree varchar(5)
	);

-- Création de la table 3G

	CREATE TABLE IF NOT EXISTS public.type_3g(
		siret varchar(14) PRIMARY KEY NOT NULL,
		date_etude date,
		type_reseau varchar(2),
		bwg varchar(5),
		sfr varchar(5),
		obs varchar(5),
		opfree varchar(5)
	);

-- Création de la table 4G

	CREATE TABLE IF NOT EXISTS public.type_4g(
		siret varchar(14) PRIMARY KEY NOT NULL,
		date_etude date,
		type_reseau varchar(2),
		bwg varchar(5),
		sfr varchar(5),
		obs varchar(5),
		opfree varchar(5)
	);
